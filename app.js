"use strict";

const express = require("express");
const bodyParser = require("body-parser");
const compression = require("compression");
const cors = require("cors")({ origin: false });
const cookieParser = require("cookie-parser")();
const awsServerlessExpressMiddleware = require("aws-serverless-express/middleware");

const app = express();
const hostname = "localhost";
const port = 3001;

app.set("json spaces", 4);

app.use(compression());
app.use(cors);
app.use(cookieParser);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(awsServerlessExpressMiddleware.eventContext());

const index = require("./src/index");
const processoTJSPRoute = require("./src/tjsp/routes/processoRoute");
const processoTJXXRoute = require("./src/tjxx/routes/processoRoute");

app.use("/", index);
app.use("/api/tjsp/v1/processo", processoTJSPRoute);
app.use("/api/tj*/v1/processo*", processoTJXXRoute);

// The aws-serverless-express library creates a server and listens on a Unix
// Domain Socket for you, so you can remove the usual call to app.listen.
const server = app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

// Export your express server so you can import it in the lambda function.
module.exports = app;
