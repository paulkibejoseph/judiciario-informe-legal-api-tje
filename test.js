exports.handler = (event, context, callback) => {
    var url = process.env.API_URL;
    var path = process.env.API_RESOURCE;
    var regex = /${process.env.REGEX}/;
    var http = require('http');
    var options = {
        hostname: url,
        port: 80,
        path: path,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }
    };

    console.log(`URL: ${url}`);
    console.log(`REGEX: ${regex}`);

    callBackFunc = function (response) {
        var str = '';

        console.log(`STATUS: ${response.statusCode}`);
        console.log(`HEADERS: ${JSON.stringify(response.headers)}`);

        response.setEncoding('utf8');

        response.on('data', function (chunk) {
            str += chunk;
        });

        response.on('end', function () {
            if (regex.test(str)) {
                console.log('API STATUS: OK!');
                context.succeed('API STATUS: OK!');
            } else {
                console.log('API STATUS: ERROR!');
                context.fail('API STATUS: ERROR!');
            }
        });

    };

    var req = http.request(options, callBackFunc);
    req.on('error', (error) => {
        console.log('REQUEST STATUS: ERROR! >>' + error);
        context.fail('REQUEST STATUS: ERROR! >>' + error);
    });
    req.write('{"opcaoPesquisa": "NUMPROC","valorConsulta": "1003978-80.2015.8.26.0506"}');
    req.end();
};