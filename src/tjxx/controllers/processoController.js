exports.get = (req, res, next) => {
  console.log(`GET: Source: ${req["baseUrl"]}/${req["path"]}`);
  console.log(`GET: Headers: `, req["headers"]);
  console.log(`GET: Params: `, req["params"]);
  res.status(404).json({
    mensagem: "Serviço em desenvolvimento. Aguarde..."
  });
};

exports.post = (req, res, next) => {
  console.log(`POST: Source: ${req["baseUrl"]}/${req["path"]}`);
  console.log(`POST: Headers: `, req["headers"]);
  console.log(`POST: Body: `, req["body"]);
  res.status(404).json({
    mensagem: "Serviço em desenvolvimento. Aguarde..."
  });
};
