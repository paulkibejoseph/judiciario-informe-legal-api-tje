const http = require('http');
const Xray = require("x-ray");

const opcoesDePesquisa = ["NUMPROC", "NMPARTE", "DOCPARTE", "NMADVOGADO", "NUMOAB", "PRECATORIA", "DOCDELEG", "NUMCDA"];

const foroDefault = '-1';
const agrupamentoDefault = 'UNIFICADO';

const xray = Xray({
    filters: {
        cleanComponent: (value) => {
            return typeof value === 'string' ? value.replace(/[\n\t\r]/g, '').trim() : value
        },
        whiteSpaces: (value) => {
            return typeof value === 'string' ? value.replace(/ +/g, ' ').trim() : value
        }
    }
});

function requisicao(req, res, url) {
    http.get(url, (resp) => {
        // console.log('statusCode: ', resp.statusCode);
        if (resp.statusCode !== 200 && resp.statusCode !== 302) {
            res.status(resp.statusCode).json({
                message: `Request Failed.\n Status Code: ${resp.statusCode}`
            });
            resp.resume();
            return;
        }

        resp.setEncoding('utf8');
        let rawData = '';
        resp.on('data', (chunk) => {
            rawData += chunk;
        });
        resp.on('end', () => {
            try {
                if (/spwTabelaMensagem/i.test(rawData)) {
                    processarMensagemRetorno(req, res, rawData);
                } else if (/listagemDeProcessos/i.test(rawData)) {
                    pesquisarProcessos(req, res, rawData);
                } else {
                    pesquisarProcesso(req, res, url);
                }
            } catch (e) {
                console.error(e.message);
                res.status(404).json({ message: e.message });
            }
        });
    }).on('error', (error) => {
        console.error(error.message);
        res.status(404).json({
            message: error.message
        });
    });
};

function pesquisarProcesso(req, res, rawData) {
    const scope = '.esajCelulaConteudoServico'
    const selector = {
        retorno: xray('#spwTabelaMensagem table[class="tabelaMensagem"] tr', {
            codigoRetorno: 404,
            tituloMensagem: xray('tr:nth-child(1) td[class="tituloMensagem"] | cleanComponent'),
            mensagemRetorno: xray('tr:nth-child(2) td[id="mensagemRetorno"] | cleanComponent')
        }),
        dadosPocesso: xray('div table[id=""].secaoFormBody tr', [{
            dado: 'td:nth-child(1)[width="150"] | cleanComponent',
            valor: 'td:nth-child(2) | cleanComponent'
        }]),
        partesPrincipais: xray('#tablePartesPrincipais .fundoClaro', [{
            dado: 'td:nth-child(1) | cleanComponent',
            valor: 'td:nth-child(2) | cleanComponent'
        }]),
        todasPartes: xray('#tableTodasPartes .fundoClaro', [{
            dado: 'td:nth-child(1) | cleanComponent',
            valor: 'td:nth-child(2) | cleanComponent'
        }]),
        ultimasMovimentacoes: xray('#tabelaUltimasMovimentacoes tr', [{
            dado: 'td:nth-child(1) | cleanComponent',
            valor: 'td:nth-child(3) | cleanComponent'
        }]),
        todasMovimentacoes: xray('#tabelaTodasMovimentacoes tr', [{
            dado: 'td:nth-child(1) | cleanComponent',
            valor: 'td:nth-child(3) | cleanComponent'
        }]),
        // peticoesDiversas: xray('#tdHistoricoDeClasses', [
        //     ['tr | cleanComponent']
        // ]),
        seccoes: xray('div[style="padding: 10px 0 10px 0;"]', [
            ['tr | cleanComponent']
        ]),
        diversas: xray('table[style="margin-left:15px; margin-top:1px;"]', [
            ['tr | cleanComponent']
        ])
    };
    xray(rawData, scope, selector)
        ((err, obj) => {
            // Object.keys(obj).forEach(key => console.log("pesquisarProcesso", key, obj[key].length));
            if (err && Object.keys(err).length > 0) {
                res.status(404).json({
                    message: err.message
                });
            } else if (obj.retorno && Object.keys(obj.retorno).length > 0) {
                res.status(200).json(obj.retorno);
            } else if (validarRetorno(obj)) {
                obj.diversas = obj.diversas.filter(data => data.length > 0 && (data.length === 1 || data[1]));
                const peticoesDiversas = [], incidentesAcoesRecursosExecucoes = [], audiencias = [], historicoClasses = [];
                const diversas = [];
                for (let index = 0; index < obj.seccoes.length; index++) {
                    const seccao = obj.seccoes[index];
                    if (seccao && seccao.indexOf('Partes do processo') !== -1) {
                        // TODO
                    } else if (seccao && seccao.indexOf('Movimentações') !== -1) {
                        // TODO
                    } else if (seccao && seccao.indexOf('Petições diversas') !== -1) {
                        obj.diversas[index].forEach(element => peticoesDiversas.push({'valor': element}));
                    } else if (seccao && seccao.indexOf('Incidentes, ações incidentais, recursos e execuções de sentenças') !== -1) {
                        obj.diversas[index].forEach(element => incidentesAcoesRecursosExecucoes.push({'valor': element}));
                    } else if (seccao && seccao.indexOf('Audiências') !== -1) {
                        obj.diversas[index].forEach(element => audiencias.push({'valor': element}));
                    } else if (seccao && seccao.indexOf('Histórico de classes') !== -1) {
                        obj.diversas[index].forEach(element => historicoClasses.push({'valor': element}));
                    }
                    diversas.push({
                        dado: obj.seccoes[index][0],
                        valor: obj.diversas[index]
                    });
                }
                const processo = Object.assign({
                    numProcesso: getNumProcesso(obj.dadosPocesso[0]["valor"]),
                    dadosPocesso: obj.dadosPocesso,
                    partesPrincipais: obj.partesPrincipais,
                    todasPartes: obj.todasPartes,
                    ultimasMovimentacoes: obj.ultimasMovimentacoes,
                    todasMovimentacoes: obj.todasMovimentacoes,
                    peticoesDiversas: peticoesDiversas,
                    incidentesAcoesRecursosExecucoes: incidentesAcoesRecursosExecucoes,
                    audiencias: audiencias,
                    historicoClasses: historicoClasses,
                    diversas: diversas
                });
                res.status(200).json(processo);
            } else {
                res.status(204).json({
                    message: 'No Content.'
                });
            }
        });
};

function pesquisarProcessos(req, res, rawData) {
    const scope = '.esajCelulaConteudoServico';
    const selector = {
        retorno: xray('#spwTabelaMensagem table[class="tabelaMensagem"] tr', {
            codigoRetorno: 404,
            tituloMensagem: xray('tr:nth-child(1) td[class="tituloMensagem"] | cleanComponent'),
            mensagemRetorno: xray('tr:nth-child(2) td[id="mensagemRetorno"] | cleanComponent')
        }),
        paginacao: xray('table[id="paginacaoSuperior"] tr', {
            resultados: 'tr:nth-child(1) td:nth-child(1) | cleanComponent',
            primeiraPagina: 'tr:nth-child(1) td:nth-child(2) div a[title="Primeira página"]@href | cleanComponent',
            paginaAnterior: 'tr:nth-child(1) td:nth-child(2) div a[title="Página anterior"]@href | cleanComponent',
            paginas: ['tr:nth-child(1) td:nth-child(2) div a[style="padding-left: 4px; padding: 5px;"]@href | cleanComponent'],
            proximaPagina: 'tr:nth-child(1) td:nth-child(2) div a[title="Próxima página"]@href | cleanComponent',
            ultimaPagina: 'tr:nth-child(1) td:nth-child(2) div a[title="Última página"]@href | cleanComponent'
        }),
        processos: xray('div[id="listagemDeProcessos"] div[id^="divProcesso"] div[class^="fundo"]', [{
            url: 'div.nuProcesso a@href',
            numProcesso: 'div.nuProcesso a | cleanComponent',
            processo: 'div.nuProcesso | cleanComponent',
            infomacoes: ['div.espacamentoLinhas | cleanComponent']
        }])
    };
    xray(rawData, scope, selector)
        ((err, obj) => {
            // Object.keys(obj).forEach(key => console.log("pesquisarProcessos", key, obj[key].length));
            if (err && Object.keys(err).length > 0) {
                res.status(404).json({
                    message: err.message
                });
            } else if (obj.retorno && Object.keys(obj.retorno).length > 0) {
                res.status(200).json(obj.retorno);
            } else if (validarRetorno(obj)) {
                res.status(200).json(Object.assign({
                    paginacao: obj.paginacao,
                    processos: obj.processos
                }));
            } else {
                res.status(204).json({
                    message: 'No Content.'
                });
            }
        });
};

function processarMensagemRetorno(req, res, rawData) {
    const scope = '.esajCelulaConteudoServico';
    const selector = {
        tituloMensagem: xray('#spwTabelaMensagem table[class="tabelaMensagem"] tr:nth-child(1) td[class="tituloMensagem"] | cleanComponent'),
        mensagemRetorno: xray('#spwTabelaMensagem table[class="tabelaMensagem"] tr:nth-child(2) td[id="mensagemRetorno"] | cleanComponent')
    };
    xray(rawData, scope, selector)
        ((err, obj) => {
            if (err && Object.keys(err).length > 0) {
                res.status(404).json({
                    message: err.message
                });
            } else if (obj && Object.keys(obj).length > 0) {
                res.status(200).json(obj);
            } else {
                res.status(204).json({
                    message: 'No Content.'
                });
            }
        });
};

function validarRetorno(obj) {
    const value = Object.keys(obj).find(key => key.indexOf('dadosPocesso') !== -1 || key.indexOf('processos') !== -1);
    return (obj && obj[value] && obj[value].length > 0);
};

function validarFiltro(filtro) {
    const opcaoPesquisa = filtro && filtro['opcaoPesquisa'] && opcoesDePesquisa.find(opcao => opcao.indexOf(filtro['opcaoPesquisa']) !== -1) || false;
    const valorConsulta = filtro && filtro['valorConsulta'] && filtro['valorConsulta'].trim().length > 0 || false;
    return opcaoPesquisa && valorConsulta;
};

function getfiltro(filtro) {
    return Object.assign({}, filtro, {
        foro: filtro.foro || foroDefault,
        agrupamento: filtro.agrupamento || agrupamentoDefault
    })
}

function pesquisar(req, res, filtro) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
    const url = 'http://esaj.tjsp.jus.br/cpopg/search.do?' +
        'conversationId=' +
        '&dadosConsulta.localPesquisa.cdLocal=' + filtro.foro +
        '&cbPesquisa=' + filtro.opcaoPesquisa +
        '&dadosConsulta.tipoNuProcesso=' +
        '&numeroDigitoAnoUnificado=' +
        '&foroNumeroUnificado=' +
        '&dadosConsulta.valorConsultaNuUnificado=' +
        '&dadosConsulta.valorConsulta=' + encodeURIComponent(filtro.valorConsulta).replace(/%20/g, '+') +
        '&uuidCaptcha=' +
        '&pbEnviar=Pesquisar';
    console.log('pesquisar-Url', url);
    if (filtro.opcaoPesquisa.indexOf('NUMPROC') !== -1) {
        pesquisarProcesso(req, res, url);
    } else {
        requisicao(req, res, url);
    }
};

function paginacao(req, res, pagina) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
    const url = 'http://esaj.tjsp.jus.br/cpopg/trocarPagina.do?' + pagina;
    requisicao(req, res, url);
}

function getNumProcesso(processo) {
    let numProcesso = processo;
    if (numProcesso && numProcesso.trim().length > 0) {
        numProcesso = numProcesso.includes("(") && numProcesso.includes(")")
            ? numProcesso.split("(")[0]
            : numProcesso;
    }
    return numProcesso;
}

module.exports = {
    validarFiltro,
    pesquisar,
    paginacao,
    getfiltro
}