'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controllers/processoController')

function processarRequisicao(req, res, filtro) {
    if (filtro && filtro.pagina && filtro.pagina.length > 0) {
        controller.paginacao(req, res, filtro.pagina);
    } else if (controller.validarFiltro(filtro)) {
        controller.pesquisar(req, res, filtro);
    } else {
        res.status(400).json({ message: 'Bad Request.' });
    }
}

router.get('/', (req, res) => {
    console.log('GET: /', 'index.html');
    res.sendFile(`${__dirname}/index.html`)
})

router.get('/pesquisa/:numProcesso', function (req, res) {
    console.log('GET: /pesquisa/:numProcesso', JSON.stringify(req.params));
    const numProcesso = req.params.numProcesso;
    processarRequisicao(req, res, controller.getfiltro({ opcaoPesquisa: 'NUMPROC', valorConsulta: numProcesso }));
});

router.get('/pesquisa/:opcaoPesquisa/:valorConsulta', function (req, res) {
    console.log('GET: /pesquisa/:opcaoPesquisa/:valorConsulta', JSON.stringify(req.params));
    const opcaoPesquisa = req.params.opcaoPesquisa;
    const valorConsulta = req.params.valorConsulta;
    processarRequisicao(req, res, controller.getfiltro({ opcaoPesquisa: opcaoPesquisa, valorConsulta: valorConsulta }));
});

router.get('/paginacao/:pagina', function (req, res) {
    console.log('GET: /paginacao/:pagina', JSON.stringify(req.params));
    const pagina = req.params.pagina;
    processarRequisicao(req, res, controller.getfiltro({pagina: pagina}));
});

router.post('/', function (req, res) {
    console.log('POST: /', JSON.stringify(req.body));
    const filtro = req.body
    processarRequisicao(req, res, controller.getfiltro(filtro));
});

module.exports = router;